/**
 * Base of all states of the application
 */
export interface BaseState<T> {
    entities: T[];
    loading: 'idle' | 'pending' | 'succeeded' | 'failed',
    errorMessage ?: string
}