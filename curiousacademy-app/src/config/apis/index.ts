const apis = {
    testimonials: {
        url: 'https://curious-academy-2340e-default-rtdb.europe-west1.firebasedatabase.app/testimonials.json'
    },
    courses: {
        url: 'https://curious-academy-2340e-default-rtdb.europe-west1.firebasedatabase.app/courses.json'
    }
};

export default apis;