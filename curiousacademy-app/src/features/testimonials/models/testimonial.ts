/**
 * Contract to get testimonial from api
 */
export interface Testimonial {
    id: number;
    name : string;
    comments: string;
    notation: number;
    avatar?: string;
    createdAt: Date;
}
