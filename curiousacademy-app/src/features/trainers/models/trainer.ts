export interface Trainer {
    key: string;
    nameSurname: string;
    description: string;
}