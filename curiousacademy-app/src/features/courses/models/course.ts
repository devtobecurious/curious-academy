export interface Course {
    key: string;
    title: string;
    description: string;
    url: string;
    duration ?: number;
}