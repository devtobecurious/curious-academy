import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { BaseState } from "../../../shared/store/states/base-state";
import { Course } from "../models/course";
import courseService from "../services/courses.service";

export interface CoursesState extends BaseState<Course>  {
}

export const initialState: CoursesState = {
    entities: [],
    loading: 'idle'
};

export const fetchCourses = createAsyncThunk(
    'courses/getAll',
    async (_, { rejectWithValue }) => {
        try {
            
            const entities = await courseService.getAll();

            return entities;
        } catch (error) {
            return rejectWithValue([]);
        }
    }
);

const courseSlices = createSlice({
    name: 'courses',
    initialState: initialState, 
    reducers: {        
    },
    extraReducers:  (builder) => {
        builder.addCase(fetchCourses.pending, (state, action) => {
            state.loading = "pending";
        });

        builder.addCase(fetchCourses.rejected, (state, action) => {
            state.loading = "failed";
            state.errorMessage = action.error.message ||'';
        });

        builder.addCase(fetchCourses.fulfilled, (state, action) => {
            state.loading = "succeeded";
            state.entities = action.payload;
        });
    }
});

export default courseSlices.reducer;