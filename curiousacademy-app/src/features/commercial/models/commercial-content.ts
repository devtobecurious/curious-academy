export interface CommercialPresentationItem {
    id: number,
    url: string;
    content: string;
}