/**
 * Language use in curious academy
 */
export interface CommercialLanguage {
    url: string;
    name: string;
    status: 'now' | 'soon';
}